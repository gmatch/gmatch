@extends('layouts.app')
@section('content')
<div class='container-fluid'>
    <div class='row'>
        
        <!-- Gmatchs / {{ $user = auth()->user() }} -->

        <div class="col-md-2">
            <div class="card text-center text-white bg-secondary">
                <div class="card-header bg-dark"> GMatchs </div>
                <div class="card-body">
                    
                    <div class="list-group">

                        @foreach($user->amigos1 as $amigo)
                            <!-- {{$idAmigo = $amigo->usuario2}} {{$nombreA = GMatch\User::find($idAmigo)->name }} -->
                            <a href="#" class="list-group-item list-group-item-primary">{{$nombreA}}</a>

                        @endforeach

                         @foreach($user->amigos2 as $amigo)
                            <!-- {{$idAmigo = $amigo->usuario1}} {{$nombreA = GMatch\User::find($idAmigo)->name }} -->
                            <a href="#" class="list-group-item list-group-item-primary">{{$nombreA}}</a>

                        @endforeach
                    
                    </div> 
                    
                    
                </div>
            </div>
        </div>

        <!-- Reseñas/Comentarios -->

        <div class="col-md-6">
            <div class="card text-center text-white bg-secondary">
                <div class="card-header bg-dark">
                    <ul class="nav nav-tabs card-header-tabs ">
                        <li class="active">
                            <a class="nav-link active" data-toggle="tab" href="#Resena">Reseñas</a>
                        </li>
                        <li >
                            <a class="nav-link" data-toggle="tab" href="#Comentario">Comentarios</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">

                    <div class="tab-content">
                        
                        <div id="Resena" class="tab-pane fade show active">
                        
                            <table class="table table-dark">
                        
                                <thead class="thead">
                                    <tr>
                                    <th scope="col">Imagen Usuario</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Calificacion</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                        
                                <tbody>

                                    @foreach ($user->reseñas as $resenna)

                                        <!-- {{$usuarioR = GMatch\User::find($resenna->usuario2)}} -->
                                        <tr>
                                            <td>    
                                                @if($usuarioR->imagen_perfil == '1')

                                                    <img src="images/1.jpg" class="img-rounded" alt="imagen perfil" style="height:100px"> 




                                                @elseif($usuarioR->imagen_perfil == '2')

                                                    <img src="images/2.jpg" class="img-rounded" alt="imagen perfil" style="height:100px"> 




                                                @elseif($usuarioR->imagen_perfil == '3')

                                                    <img src="images/3.jpg" class="img-rounded" alt="imagen perfil" style="height:100px"> 




                                                @elseif($usuarioR->imagen_perfil == '4')

                                                    <img src="images/4.jpg" class="img-rounded" alt="imagen perfil" style="height:100px"> 



                                                @elseif($usuarioR->imagen_perfil == '5')

                                                    <img src="images/5.jpg" class="img-rounded" alt="imagen perfil" style="height:100px"> 




                                                @elseif($usuarioR->imagen_perfil == '6')

                                                    <img src="images/6.jpg" class="img-rounded" alt="imagen perfil" style="height:100px" > 




                                                @endif
                                            </td>
                                            <td>{{$usuarioR->name}}</td>
                                            <td>{{$resenna->calificacion}}</td>
                                            <td>{{$resenna->comentario}}</td>
                                            <td>
                                                <a href="{{ route('modificarResena',['resenna' => $resenna])}}" class="btn btn-primary"> Editar </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('eliminarResena',['resenna' => $resenna])}}" class="btn btn-danger"> Eliminar </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                                

                            </table>

                        </div>

                    
                                
                        <div id="Comentario" class='tab-pane fade'>


                            <table class="table table-dark">
                                <thead class="thead">
                                    <tr>
                                    
                                    <th scope="col">SubForo</th>
                                    <th scope="col">Comentario</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($user->comentarios as $comentario)

                                        <!-- {{$subforoC = GMatch\subforo::find($comentario->subforo_id)}} -->
                                    
                                            <td>{{$subforoC->nombre}}</td>
                                            <td>{{$comentario->comentario}}</td>
                                            <td>
                                                <a href="{{ route('modificarComentario',['comentario' => $comentario])}}" class="btn btn-primary"> Editar </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('eliminarComentario',['comentario' => $comentario])}}" class="btn btn-danger"> Eliminar </a>
                                            </td>
                                        </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <!-- Info Perfil -->

        <div class="col-md-4">
            <div class="card text-center text-white bg-secondary">
                <div class="card-header bg-dark"> Informacion Perfil </div>
                <div class="card-body">

                    <div>
                        @if($user->imagen_perfil == '1')

                            <img src="images/1.jpg" class="img-rounded" alt="imagen perfil" style="height:300px"> 


                        

                        @elseif($user->imagen_perfil == '2')

                            <img src="images/2.jpg" class="img-rounded" alt="imagen perfil" style="height:300px"> 


                        

                        @elseif($user->imagen_perfil == '3')

                            <img src="images/3.jpg" class="img-rounded" alt="imagen perfil" style="height:300px"> 


                        

                        @elseif($user->imagen_perfil == '4')

                            <img src="images/4.jpg" class="img-rounded" alt="imagen perfil" style="height:300px"> 


                        
                        @elseif($user->imagen_perfil == '5')

                            <img src="images/5.jpg" class="img-rounded" alt="imagen perfil" style="height:300px"> 


                        

                        @elseif($user->imagen_perfil == '6')

                            <img src="images/6.jpg" class="img-rounded" alt="imagen perfil" style="height:300px" > 


                        

                        @endif
                    </div>

                    <br>

                    <div>
                        <p> {{$user->name}} </p>
                    </div>


                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
