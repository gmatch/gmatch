<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class juegos extends Model
{
    protected $fillable = [
        'nombre', 'tipos_id' ];
    public function  usuarios_juegos(){
        return $this->hasMany(usuario_juego::class);
    }
    public function foro(){
        return $this->hasOne(Foro::class);
    }
    public function tipo(){
        return $this->belongsTo(tipo::class);
    }
    
}
