<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class tipo extends Model
{
    protected $fillable = [
        'nombre' ];
    public function juegos(){
        return $this->hasOne(juegos::class);
    }
}
