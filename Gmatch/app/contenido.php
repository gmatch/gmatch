<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class contenido extends Model
{
    protected $fillable = [
        'name', 'Nickname','video','foto1','foto2','foto3','Division','Horas_de_juego','type','Descripcion'];
    public function  usuarios_juegos(){
        return $this->hasMany(usuario_juego::class);
    } 
}
