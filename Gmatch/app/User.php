<?php

namespace GMatch;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','edad','microfono','Identidad_de_Genero','type'  ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const admin_type='admin';
    const default_type='gamer';

    public function isAdmin(){
        return $this->type === self::admin_type;
    }

    public function amigos1(){
        return $this->hasMany(amigos::class,"usuario1");
    }
    public function amigos2(){
        return $this->hasMany(amigos::class,"usuario2");
    }
    
    
    
    public function  usuarios_juegos(){
        return $this->hasMany(usuario_juego::class);
    }
    public function reseñas(){
        return $this->hasMany(resenna::class,"usuario");
    }

    public function comentarios(){
        return $this->hasMany(comentarios::class,"users_id");
    }
    

    

}
