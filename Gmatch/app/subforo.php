<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class subforo extends Model
{
    protected $fillable = [
        'nombre', 'foros_id' ];
    public function foro(){
        return $this->belongsTo(foro::class);
    }
    public function comentarios(){
        return $this->hasMany(comentarios::class);
    }
}
