<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class usuario_juego extends Model
{
    protected $fillable = [
        'user_id', 'juego_id','contenido_id' ];
    public function juego(){
        return $this->belongsTo(juegos::class);
    }
    public function contenido(){
        return $this->belongsTo(contenido::class);
    }
    public function usuario(){
        return $this->belongsTo(User::class);
    }
}
