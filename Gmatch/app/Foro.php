<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class Foro extends Model
{
    protected $fillable = [
        'nombre', 'descripcion','juegos_id' ];
    public function juego(){
        return $this->belongsTo(Foro::class);
    }
    public function subforo(){
        return $this->hasMany(subforo::class);
    }
}
