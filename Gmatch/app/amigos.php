<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class amigos extends Model
{
    protected $fillable = [
        'usuario1', 'usuario2' ];
    public function usuario1(){
        return $this->belongsTo(User::class,"usuario1");
        
    }
    public function usuario2(){
        return $this->belongsTo(User::class,"usuario2");
        
    }
}

