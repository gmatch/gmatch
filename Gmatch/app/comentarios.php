<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class comentarios extends Model
{
    protected $fillable = [
        'comentario', 'subforo_id','users_id' ];
    public function subforo(){
        return $this->belongsTo(subforo::class);
    }
    public function usuario(){
        return $this->belongsTo(User::class);
    }
}
