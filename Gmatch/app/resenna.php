<?php

namespace GMatch;

use Illuminate\Database\Eloquent\Model;

class resenna extends Model
{
    protected $fillable = [
        'calificacion', 'comentario','usuario','usuario2' ];
    public function usuario(){
        return $this->belongsTo(User::class,"usuario");
    }

    public function usuario2(){
        return $this->belongsTo(User::class,"usuario2");
    }
}
