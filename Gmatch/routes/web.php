<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/perfil', 'PerfilController@perfil')->name('perfil');

/* Resenas */

Route::get('/modificarResena{id}', 'ResenaController@modificarResena')->name('modificarResena');

Route::get('/eliminarResena{id}', 'ResenaController@eliminarResena')->name('eliminarResena');

/* Comentarios */

Route::get('/modificarComentario{id}', 'ComentarioController@modificarComentario')->name('modificarComentario');

Route::get('/perfil{id}', 'ComentarioController@eliminarComentario')->name('eliminarComentario');
