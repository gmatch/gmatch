<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContenidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contenidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('Nickname');
            $table->string('video');
            $table->string('foto1');
            $table->string('foto2');
            $table->string('foto3');
            $table->string('Division');
            $table->integer('Horas_de_juego');
            $table->enum('type',['Diversion','Competitivo','Otro'])->default('Otro');            
            $table->mediumText('Descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenidos');
    }
}
