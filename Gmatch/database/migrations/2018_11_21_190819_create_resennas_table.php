<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResennasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resennas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('calificacion');
            $table->string('comentario');
            $table->integer('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users')->onDelete('cascade');
            $table->integer('usuario2')->unsigned();
            $table->foreign('usuario2')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resennas');
    }
}
