<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class ContenidosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $nombre_juegos = DB::table('juegos')->pluck('nombre');
        $divisiones = array("Bronze","Plata","Oro","Platino","Diamante","Maestro","Retador", "Plata","Nova","AK","Sheriff","Aguila","Master Guardian","Global Elite");
        
        
    	foreach (range(1,100) as $index) {
            
            $division = $faker->randomElement($divisiones);

            DB::table('contenidos')->insert([
        
	            'name' => $faker->randomElement($nombre_juegos),
	            'nickname' => $faker->word,
                'video' => $faker->paragraph(1,true),
                'foto1' => $faker->imageUrl(800, 600, 'cats')  , 
                'foto2' => $faker->imageUrl(800, 600, 'cats')  ,
                'foto3' => $faker->imageUrl(800, 600, 'cats')  ,
                'division' => $division ,
                'Horas_de_juego' => $faker-> numberBetween(0,1000),
                'type' => $faker->randomElement(array("Diversion","Competitivo","Otro")),
                'descripcion' =>$faker->paragraph(3,true),

            ]);
            
        }
    }
}
