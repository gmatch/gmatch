<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ResenasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $usuarios = DB::table('users')->pluck('id');
        
    	foreach (range(1,100) as $index) {
            $usuario = $faker->randomElement($usuarios);
            $usuario2 = $faker->randomElement($usuarios);
            while ($usuario == $usuario2) {

                $usuario2 = $faker->randomElement($usuarios);

            }

	        DB::table('resennas')->insert([
                'calificacion' => $faker->numberBetween(1,10),
                'comentario' => $faker->paragraph(1,true),
                'usuario' => $usuario,
                'usuario2'=> $usuario2,
                
                
            ]);
            
        }
    }
}
