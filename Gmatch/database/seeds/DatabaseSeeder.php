<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeed::class);
         

         $this->call(AmigosSeed::class);

         $this->call(TipoSeed::class);
         
         $this->call(JuegosSeed::class);
         
         $this->call(ResenasSeed::class);
         
         $this->call(ForosSeed::class);

         $this->call(SubforosSeed::class);
         
         $this->call(ContenidosSeed::class);
         
         $this->call(ComentariosSeed::class);
         
         $this->call(UsuarioJuegoSeed::class);
    }
}
