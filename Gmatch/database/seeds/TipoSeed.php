<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TipoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = array('Accion','Arcade','Deportivo','Estrategia','Simulacion','Aventura','Supervivencia','MMORPG','Cooperativo','Social');
    	foreach (range(0,9) as $index) {
	        DB::table('tipos')->insert([
	            'nombre' => $tipos[$index],
	            
            ]);
            
        }
    }
}
