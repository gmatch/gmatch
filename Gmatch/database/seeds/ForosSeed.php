<?php
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;

class ForosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $id_juegos = DB::table('juegos')->pluck('id');
        $nombre_juegos = DB::table('juegos')->pluck('nombre');
        
        foreach (range(0,sizeof($id_juegos)-1) as $index) {
	        DB::table('foros')->insert([
	            'nombre' => $nombre_juegos[$index],
	            'descripcion' => $faker->paragraph(1,true),
                'juegos_id' =>  $id_juegos[$index],
            ]);
            
        }
    }
}
