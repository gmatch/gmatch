<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('users')->insert([
	            'name' => $faker->name,
	            'email' => $faker->email,
                'password' => bcrypt('secret'),
                'edad'=> $faker->numberBetween(18,85),
                'microfono' => $faker->numberBetween(-1,2),
                'Identidad_de_Genero' => $faker-> randomElement(array('Hombre','Mujer','Otro','No indica','Prefiero no decirlo')),
                'type' => $faker-> randomElement(array('admin','gamer')),
                "imagen_perfil" => $faker -> numberBetween(1,6),
            ]);
            
        }
    }
}
