<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class ComentariosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        $subforos = DB::table('subforos')->pluck('id');
        $usuarios = DB::table('users')->pluck('id');
        
    	foreach (range(1,100) as $index) {
            
            DB::table('comentarios')->insert([
        
                'comentario' => $faker->paragraph(3,true),
                'subforo_id' => $faker -> randomElement($subforos),
                'users_id'=> $faker -> randomElement($usuarios),

            ]);
            
        }
    }
}
