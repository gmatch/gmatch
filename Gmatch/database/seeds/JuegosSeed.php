<?php

use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;

class JuegosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = DB::table('tipos')->pluck('id');
        $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('juegos')->insert([
                'nombre' => $faker->sentence(3,true),
                'tipos_id' => $faker->randomElement($tipos),
                
                
            ]);
            
        }
    }
}
