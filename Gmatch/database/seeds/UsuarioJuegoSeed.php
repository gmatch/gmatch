<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use Illuminate\Support\Facades\DB;
class UsuarioJuegoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $usuarios = DB::table('users')->pluck('id');
        $juegos = DB::table('juegos')->pluck('id');
        $contenidos = DB::table('contenidos')->pluck('id');
    	foreach (range(1,100) as $index) {
	        DB::table('usuario_juegos')->insert([
                
                "user_id"=>$faker->randomElement($usuarios),
                "juego_id"=>$faker->randomElement($juegos),
                "contenido_id"=>$faker->randomElement($contenidos),

            ]);
            
        }
    }
}
