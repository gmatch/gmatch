<?php
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SubforosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $foros = DB::table('foros')->pluck('id');
    	foreach (range(1,100) as $index) {
	        DB::table('subforos')->insert([
                'nombre' => $faker->word,
                'foros_id' => $faker->randomElement($foros),
                
            ]);
            
        }
    }
}
