<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class AmigosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $usuarios = DB::table('users')->pluck('id');
        
    	foreach (range(1,100) as $index) {
            $usuario = $faker->randomElement($usuarios);
            $usuario2 = $faker->randomElement($usuarios);
            while ($usuario == $usuario2) {

                $usuario2 = $faker->randomElement($usuarios);

            }

	        DB::table('amigos')->insert([
                'usuario1' => $usuario,
                'usuario2'=> $usuario2,
                
                
            ]);
            
        }
    }
}
